package bzh.coleb.application;

import bzh.coleb.domain.StarStateService;
import bzh.coleb.domain.StarStateServiceImpl;
import bzh.coleb.domain.api.BikeDTO;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import reactor.core.publisher.Mono;

import java.util.List;

@Controller(value = "star-state", consumes = MediaType.APPLICATION_JSON)
public class StarController {

    private final StarStateService starStateService;

    public StarController(StarStateServiceImpl starService) {
        this.starStateService = starService;
    }

    @Get(value = "/bikes")
    public Mono<List<BikeDTO>> getBikesStationsStates() {
        return Mono.from(starStateService.getBikesStationsStates());
    }

    @Get(value = "bikes/{name}")
    public Mono<BikeDTO> getBikeStationState(String name) {
        return Mono.from(starStateService.getBikeStationState(name));
    }

}
