package bzh.coleb.domain.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BikeDTO implements Serializable {

    private String nom;

    private String etat;

    private Integer velosDisponible;

    private Integer parkingsDisponible;

}
