package bzh.coleb.domain.spi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FieldsStarDTO implements Serializable {

    private String nom;

    private String etat;

    private String nombrevelosdisponibles;

    private String nombreemplacementsdisponibles;

}
