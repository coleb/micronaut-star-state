package bzh.coleb.domain.spi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class BikeStarDTO implements Serializable {

    private String nhits;

    private List<RecordStarDTO> records;

}
