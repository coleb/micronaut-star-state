package bzh.coleb.domain.spi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RecordStarDTO implements Serializable {

    private String recordid;

    private FieldsStarDTO fields;

}
