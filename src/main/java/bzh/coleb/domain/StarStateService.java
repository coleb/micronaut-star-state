package bzh.coleb.domain;

import bzh.coleb.domain.api.BikeDTO;
import org.reactivestreams.Publisher;

import java.util.List;

public interface StarStateService {
    Publisher<List<BikeDTO>> getBikesStationsStates();

    Publisher<BikeDTO> getBikeStationState(String name);
}
