package bzh.coleb.domain;

import bzh.coleb.domain.api.BikeDTO;
import bzh.coleb.domain.spi.RecordStarDTO;
import bzh.coleb.infrastructure.StarApi;
import jakarta.inject.Singleton;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class StarStateServiceImpl implements StarStateService {

    private StarApi starApi;

    StarStateServiceImpl(StarApi starApi) {
        this.starApi = starApi;
    }

    @Override
    public Publisher<List<BikeDTO>> getBikesStationsStates() {
        return Mono.from(starApi.getBikesStates()).map(bikeStarDTO -> bikeStarDTO.getRecords()
                .stream()
                .map(RecordStarDTO::getFields)
                .map(station -> BikeDTO.builder()
                        .nom(station.getNom())
                        .etat(station.getEtat())
                        .velosDisponible(Integer.valueOf(station.getNombrevelosdisponibles()))
                        .parkingsDisponible(Integer.valueOf(station.getNombreemplacementsdisponibles()))
                        .build())
                .collect(Collectors.toList()
                )
        );
    }

    @Override
    public Publisher<BikeDTO> getBikeStationState(String name) {
        return Mono.from(getBikesStationsStates()).map(bikesStations -> bikesStations
                .stream()
                .filter(station -> name.equals(station.getNom()))
                .findFirst()
                .orElse(BikeDTO.builder().build()));
    }
}
