package bzh.coleb.infrastructure;

import bzh.coleb.domain.spi.BikeStarDTO;
import bzh.coleb.infrastructure.config.StarConstant;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;
import org.reactivestreams.Publisher;

@Client(StarConstant.API)
public interface StarApi {

    @Get(StarConstant.BIKES)
    Publisher<BikeStarDTO> getBikesStates();

}