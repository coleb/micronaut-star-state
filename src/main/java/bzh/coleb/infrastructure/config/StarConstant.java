package bzh.coleb.infrastructure.config;

public class StarConstant {

    public static final String API = "https://data.explore.star.fr/api/records/1.0/search/";

    public static final String BIKES = "?dataset=vls-stations-etat-tr&q=&facet=nom&facet=etat&facet=nombreemplacementsactuels&facet=nombreemplacementsdisponibles&facet=nombrevelosdisponibles";
    
}
